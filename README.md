# vhifem

Vectorized Homogenization with Image-based FEM

# Intro

vhifem is a memory-efficient MATALB program for the numerical homogenization of heterogeneous materials, charactierized by image-based models. The Finite Element Method is employed following an assembly-free approach, that is, no global matrix is stored. The developed solver employs vectorization analogously to massively parallel strategies, eliminating all strenuous for loops.

Two physical phenomena are considered, thermal conduction and linear elasticity. The governing partial differential equations solved with vhifem are Poisson's Equation and Navier's Equation. Simulations can be run in 2D or 3D.

# How to use the program

There are two ways to run vhifem, detailed in the following items. The output always is a matrix representation of the constitutive tensor C.

1) Calling the vhifem function. Two inputs are needed, strings indicating which JSON and RAW files should be read. Those contain, respectively, model parameters, and an 8-bit grayscale image of the domain. Both files can be generated from a .TIFF image via the [pytomoviewer][pytomoviewer_link] application.

```
C = vhifem(json_file,raw_file); 
```

[pytomoviewer_link]: https://github.com/LCC-UFF/pytomoviewer

2) Calling directly the solver for a specific physical phenomenom. For instance, considering 2D thermal conduction:

```
C = vhifem_Thermal_2D([nRows,nCols],matMap,matProp,pcgTol,pcgIter); 
```
Where nRows and nCols are the numbers of rows and columns on a given image, matMap is an array that associates element indexes with material keys (this is the image itself!), matProp is an array that associates isotropic conductivity properties to each material key (color value), pcgTol is the numeric tolerance for dimensionless norms of residuals from the PCG method, and pcgIter is the maximum number of iterations.

For other types of analysis, the input is analogous to what is described above. Comments above the function signature in the files [vhifem_Thermal_2D.m][thermal2D_link], [vhifem_Thermal_3D.m][thermal3D_link], [vhifem_Elastic_2D.m][elastic2D_link], and [vhifem_Elastic_3D.m][elastic3D_link] specify the details.

[thermal2D_link]: https://gitlab.com/cortezpedro/vhifem/-/blob/main/src/vhifem_Thermal_2D.m
[thermal3D_link]: https://gitlab.com/cortezpedro/vhifem/-/blob/main/src/vhifem_Thermal_3D.m
[elastic2D_link]: https://gitlab.com/cortezpedro/vhifem/-/blob/main/src/vhifem_Elastic_2D.m
[elastic3D_link]: https://gitlab.com/cortezpedro/vhifem/-/blob/main/src/vhifem_Elastic_3D.m
