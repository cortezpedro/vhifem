%%-------------------------------------------------------------------------
% Vectorized Homogenization with Image-based FEM (Elasticity_2D)
% Input:
% * dim        = [n_rows,n_cols]
% * elemMatMap = array with material keys (this is the image)
% * matProp    = array with the isotropic elasticity coeffs [E,v] of each phase
% * pcgTol     = dimensionless tolerance for the PCG method
% * pcgIter    = max number of iterations for the PCG method
% Output:
% * C = matrix representation of the homogenized constitutive tensor
%%-------------------------------------------------------------------------
function C = vhifem_Elastic_2D(dim,elemMatMap,matProp,pcgTol,pcgIter)
    C = zeros(3,3);
    %% Generate DOF map
    nElems = uint64(length(elemMatMap)); nDOFs = uint64(2*nElems); nRows = dim(1);
    DOFMap = zeros(dim(1)+1,dim(2)+1,'uint64');
    DOFMap(1:dim(1),1:dim(2)) = reshape(1:(nDOFs)/2,dim(1),dim(2));
    DOFMap(dim(1)+1,:) = DOFMap(1,:); DOFMap(:,dim(2)+1) = DOFMap(:,1);
    DOFMap = 2*DOFMap(:)-1;
    %% Initialize auxiliary arrays (for vectorized indexing and gathering)
    ee = 1:nElems; n = ee + uint64(floor(double(ee-1)/double(nRows))) + 1;
    g = zeros(nElems,1,'double');
    clear ee
    %% Compute local matrices (analytical solutions for 1x1 Q4 element)
    nMat = uint16(size(matProp,1));
    E = reshape(matProp(1:nMat,1),1,1,nMat);
    v = reshape(matProp(1:nMat,2),1,1,nMat);
    c_1 = (E.*(4*v - 3))./(6*(2*v - 1).*(v + 1));
    c_2 = E./(9*(2*v - 1)) - (5*E)./(36*(v + 1));
    c_3 = E./(8*(2*v - 1).*(v + 1));
    c_4 = (E.*(4*v - 1))./(8*(2*v - 1).*(v + 1));
    c_5 = -(E.*(2*v - 3))./(12*(2*v - 1).*(v + 1));
    c_6 = -(E.*(4*v - 3))./(12*(2*v - 1).*(v + 1));
    c_7 = -(E.*v)./(6*(2*v - 1).*(v + 1));
    K = [  c_1, -c_3,  c_2, -c_4,  c_6,  c_3,  c_7,  c_4 ;
          -c_3,  c_1,  c_4,  c_7,  c_3,  c_6, -c_4,  c_5 ;
           c_2,  c_4,  c_1,  c_3,  c_7, -c_4,  c_6, -c_3 ;
          -c_4,  c_7,  c_3,  c_1,  c_4,  c_5, -c_3,  c_6 ;
           c_6,  c_3,  c_7,  c_4,  c_1, -c_3,  c_5, -c_4 ;
           c_3,  c_6, -c_4,  c_5, -c_3,  c_1,  c_4,  c_7 ;
           c_7, -c_4,  c_6, -c_3,  c_5,  c_4,  c_1,  c_3 ;
           c_4,  c_5, -c_3,  c_6, -c_4,  c_7,  c_3,  c_1 ];
    c_1 = (E.*(v - 1))./(2*(2*v - 1).*(v + 1));
    c_2 = (E.*v)./(2*(2*v - 1).*(v + 1));
    c_3 = E./(4*(v + 1));
    B = [ -c_1,  c_2,  c_1,  c_2,  c_1, -c_2, -c_1, -c_2 ;
           c_2, -c_1, -c_2, -c_1, -c_2,  c_1,  c_2,  c_1 ;
          -c_3, -c_3, -c_3,  c_3,  c_3,  c_3,  c_3, -c_3 ];
    clear c_1 c_2 c_3 c_4 c_5 c_6 c_7 E v
    %% Jacobi preconditioner
    M = zeros(nDOFs,1,'double');
    g(:) = K(1,1,elemMatMap); M(DOFMap(n))   = M(DOFMap(n))   + g;
    g(:) = K(2,2,elemMatMap); M(DOFMap(n)+1) = M(DOFMap(n)+1) + g;
    n = n + nRows+1;
    g(:) = K(3,3,elemMatMap); M(DOFMap(n))   = M(DOFMap(n))   + g;
    g(:) = K(4,4,elemMatMap); M(DOFMap(n)+1) = M(DOFMap(n)+1) + g;
    n = n - 1;
    g(:) = K(5,5,elemMatMap); M(DOFMap(n))   = M(DOFMap(n))   + g;
    g(:) = K(6,6,elemMatMap); M(DOFMap(n)+1) = M(DOFMap(n)+1) + g;
    n = n - (nRows+1);
    g(:) = K(7,7,elemMatMap); M(DOFMap(n))   = M(DOFMap(n))   + g;
    g(:) = K(8,8,elemMatMap); M(DOFMap(n)+1) = M(DOFMap(n)+1) + g;
    n = n + 1;
    M = M.^-1;
    %% Homogenization (ii=1 -> x, ii=2 -> y, ii=3 -> xy(shear))
    r = zeros(nDOFs,1,'double');
    x = zeros(nDOFs,1,'double');
    d = zeros(nDOFs,1,'double');
    q = zeros(nDOFs,1,'double');
    for ii=1:3
        %% Compute RHS - Domain
        r(:) = 0;
        g(:) = B(ii,1,elemMatMap); r(DOFMap(n))   = r(DOFMap(n))   + g;
        g(:) = B(ii,2,elemMatMap); r(DOFMap(n)+1) = r(DOFMap(n)+1) + g;
        n = n + nRows+1;
        g(:) = B(ii,3,elemMatMap); r(DOFMap(n))   = r(DOFMap(n))   + g;
        g(:) = B(ii,4,elemMatMap); r(DOFMap(n)+1) = r(DOFMap(n)+1) + g;
        n = n - 1;
        g(:) = B(ii,5,elemMatMap); r(DOFMap(n))   = r(DOFMap(n))   + g;
        g(:) = B(ii,6,elemMatMap); r(DOFMap(n)+1) = r(DOFMap(n)+1) + g;
        n = n - (nRows+1);
        g(:) = B(ii,7,elemMatMap); r(DOFMap(n))   = r(DOFMap(n))   + g;
        g(:) = B(ii,8,elemMatMap); r(DOFMap(n)+1) = r(DOFMap(n)+1) + g;
        n = n + 1;
        %% Solve system (PCG - Elem by elem)
        x(:) = 0;                             % x0 = [0]
        d(:) = M.*r;                          % s  = M^-1 * r (d=s on first iteration)
        delta = dot(r,d); delta0 = delta; delta_prev = delta; % delta = dot(r,s)
        if (abs(delta)>10^-12) % check if initial guess did not already satisfy an absolute tolerance
            it = uint64(0);
            while (it < pcgIter), it = it+1;
                % q = K * d
                q(:) = 0;
                g(:) = K(1,1,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n));
                g(:) = K(1,2,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n)+1);
                g(:) = K(1,3,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n+(nRows+1)));
                g(:) = K(1,4,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n+(nRows+1))+1);
                g(:) = K(1,5,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n+nRows));
                g(:) = K(1,6,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n+nRows)+1);
                g(:) = K(1,7,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n-1));
                g(:) = K(1,8,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n-1)+1);
                g(:) = K(2,1,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n));
                g(:) = K(2,2,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n)+1);
                g(:) = K(2,3,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n+(nRows+1)));
                g(:) = K(2,4,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n+(nRows+1))+1);
                g(:) = K(2,5,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n+nRows));
                g(:) = K(2,6,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n+nRows)+1);
                g(:) = K(2,7,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n-1));
                g(:) = K(2,8,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n-1)+1);
                n = n+(nRows+1);
                g(:) = K(3,1,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n-(nRows+1)));  
                g(:) = K(3,2,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n-(nRows+1))+1);
                g(:) = K(3,3,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n));           
                g(:) = K(3,4,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n)+1);          
                g(:) = K(3,5,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n-1));         
                g(:) = K(3,6,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n-1)+1);        
                g(:) = K(3,7,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n-(nRows+2)));
                g(:) = K(3,8,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n-(nRows+2))+1);
                g(:) = K(4,1,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n-(nRows+1)));  
                g(:) = K(4,2,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n-(nRows+1))+1);
                g(:) = K(4,3,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n));            
                g(:) = K(4,4,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n)+1);          
                g(:) = K(4,5,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n-1));          
                g(:) = K(4,6,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n-1)+1);        
                g(:) = K(4,7,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n-(nRows+2)));
                g(:) = K(4,8,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n-(nRows+2))+1);
                n = n-1;
                g(:) = K(5,1,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n-nRows));     
                g(:) = K(5,2,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n-nRows)+1);   
                g(:) = K(5,3,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n+1));         
                g(:) = K(5,4,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n+1)+1);       
                g(:) = K(5,5,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n));           
                g(:) = K(5,6,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n)+1);         
                g(:) = K(5,7,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n-(nRows+1))); 
                g(:) = K(5,8,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n-(nRows+1))+1);
                g(:) = K(6,1,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n-nRows));     
                g(:) = K(6,2,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n-nRows)+1);   
                g(:) = K(6,3,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n+1));         
                g(:) = K(6,4,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n+1)+1);       
                g(:) = K(6,5,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n));           
                g(:) = K(6,6,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n)+1);         
                g(:) = K(6,7,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n-(nRows+1))); 
                g(:) = K(6,8,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n-(nRows+1))+1);
                n = n-(nRows+1);
                g(:) = K(7,1,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n+1));          
                g(:) = K(7,2,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n+1)+1);        
                g(:) = K(7,3,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n+(nRows+2)));  
                g(:) = K(7,4,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n+(nRows+2))+1);
                g(:) = K(7,5,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n+(nRows+1)));  
                g(:) = K(7,6,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n+(nRows+1))+1);
                g(:) = K(7,7,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n));            
                g(:) = K(7,8,elemMatMap); q(DOFMap(n))   = q(DOFMap(n))   + g.*d(DOFMap(n)+1);          
                g(:) = K(8,1,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n+1));          
                g(:) = K(8,2,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n+1)+1);        
                g(:) = K(8,3,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n+(nRows+2)));  
                g(:) = K(8,4,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n+(nRows+2))+1);
                g(:) = K(8,5,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n+(nRows+1)));  
                g(:) = K(8,6,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n+(nRows+1))+1);
                g(:) = K(8,7,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n));            
                g(:) = K(8,8,elemMatMap); q(DOFMap(n)+1) = q(DOFMap(n)+1) + g.*d(DOFMap(n)+1);          
                n = n + 1;
                a = delta / dot(d,q);         % a  = delta/dot(d,q)
                x = x + a*d;                  % x += a*d
                r = r - a*q;                  % r -= a*q
                q = M.*r;                     % s  = M^-1 * r (use q to store s)
                delta = dot(r,q);             % delta = dot(r,s)
                if (delta <= (delta0 * pcgTol^2)), break; end
                d = q + (delta/delta_prev)*d; % d = s + (delta/delta_prev)*d
                delta_prev = delta;
            end
            fprintf("PCG ran through %i steps.\nAdimensional residual: %e\n",it,sqrt(delta/delta0));
        else
            fprintf("Null solution satisfied PCG\n");
        end
        %% Update column ii of C
        qx = 0; qy = 0; qxy = 0; displ = zeros(8,1,'double'); displ([3,5]*(ii==1)+[6,8]*(ii==2)+[5,7]*(ii==3),:) = [1;1];
        g(:) = B(1,1,elemMatMap); qx  = qx  + sum(g.*(displ(1)-x(DOFMap(n))));
        g(:) = B(2,1,elemMatMap); qy  = qy  + sum(g.*(displ(1)-x(DOFMap(n))));
        g(:) = B(3,1,elemMatMap); qxy = qxy + sum(g.*(displ(1)-x(DOFMap(n))));
        g(:) = B(1,2,elemMatMap); qx  = qx  + sum(g.*(displ(2)-x(DOFMap(n)+1)));
        g(:) = B(2,2,elemMatMap); qy  = qy  + sum(g.*(displ(2)-x(DOFMap(n)+1)));
        g(:) = B(3,2,elemMatMap); qxy = qxy + sum(g.*(displ(2)-x(DOFMap(n)+1)));
        n = n+(nRows+1);
        g(:) = B(1,3,elemMatMap); qx  = qx  + sum(g.*(displ(3)-x(DOFMap(n))));
        g(:) = B(2,3,elemMatMap); qy  = qy  + sum(g.*(displ(3)-x(DOFMap(n))));
        g(:) = B(3,3,elemMatMap); qxy = qxy + sum(g.*(displ(3)-x(DOFMap(n))));
        g(:) = B(1,4,elemMatMap); qx  = qx  + sum(g.*(displ(4)-x(DOFMap(n)+1)));
        g(:) = B(2,4,elemMatMap); qy  = qy  + sum(g.*(displ(4)-x(DOFMap(n)+1)));
        g(:) = B(3,4,elemMatMap); qxy = qxy + sum(g.*(displ(4)-x(DOFMap(n)+1)));
        n = n-1;
        g(:) = B(1,5,elemMatMap); qx  = qx  + sum(g.*(displ(5)-x(DOFMap(n))));
        g(:) = B(2,5,elemMatMap); qy  = qy  + sum(g.*(displ(5)-x(DOFMap(n))));
        g(:) = B(3,5,elemMatMap); qxy = qxy + sum(g.*(displ(5)-x(DOFMap(n))));
        g(:) = B(1,6,elemMatMap); qx  = qx  + sum(g.*(displ(6)-x(DOFMap(n)+1)));
        g(:) = B(2,6,elemMatMap); qy  = qy  + sum(g.*(displ(6)-x(DOFMap(n)+1)));
        g(:) = B(3,6,elemMatMap); qxy = qxy + sum(g.*(displ(6)-x(DOFMap(n)+1)));
        n = n-(nRows+1);
        g(:) = B(1,7,elemMatMap); qx  = qx  + sum(g.*(displ(7)-x(DOFMap(n))));
        g(:) = B(2,7,elemMatMap); qy  = qy  + sum(g.*(displ(7)-x(DOFMap(n))));
        g(:) = B(3,7,elemMatMap); qxy = qxy + sum(g.*(displ(7)-x(DOFMap(n))));
        g(:) = B(1,8,elemMatMap); qx  = qx  + sum(g.*(displ(8)-x(DOFMap(n)+1)));
        g(:) = B(2,8,elemMatMap); qy  = qy  + sum(g.*(displ(8)-x(DOFMap(n)+1)));
        g(:) = B(3,8,elemMatMap); qxy = qxy + sum(g.*(displ(8)-x(DOFMap(n)+1)));
        n = n + 1;
        C(:,ii) = [ qx ; qy ; qxy ] / double(nElems);
    end    
end
