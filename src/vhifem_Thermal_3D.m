%%-------------------------------------------------------------------------
% Vectorized Homogenization with Image-based FEM (Thermal_3D)
% Input:
% * dim        = [n_rows,n_cols,n_layers]
% * elemMatMap = array with material keys (this is the image)
% * matProp    = array with the isotropic conductivity of each phase
% * pcgTol     = dimensionless tolerance for the PCG method
% * pcgIter    = max number of iterations for the PCG method
% Output:
% * C = matrix representation of the homogenized constitutive tensor
%%-------------------------------------------------------------------------
function C = vhifem_Thermal_3D(dim,elemMatMap,matProp,pcgTol,pcgIter)
    C = zeros(3,3);
    %% Generate DOF map
    nElems = uint64(length(elemMatMap)); nDOFs = nElems;
    nRows = dim(1); nCols = dim(2); nNodes_xy = (nRows+1)*(nCols+1);
    DOFMap = zeros(nRows+1,nCols+1,dim(3)+1,'uint64');
    DOFMap(1:nRows,1:nCols,1:dim(3)) = reshape(1:nDOFs,nRows,nCols,dim(3));
    DOFMap(nRows+1,:,:) = DOFMap(1,:,:); DOFMap(:,nCols+1,:) = DOFMap(:,1,:); DOFMap(:,:,dim(3)+1) = DOFMap(:,:,1);
    DOFMap = DOFMap(:);
    %% Initialize auxiliary arrays (for vectorized indexing and gathering)
    ee = 1:nElems; n = mod(ee-1,nCols*nRows);
    n  = 2+n+uint64(floor(double(n)/double(nRows)))+uint64(floor(double(ee-1)/double(nCols*nRows)))*(nNodes_xy);
    g  = zeros(nElems,1,'double');
    clear ee
    %% Compute local matrices (analytical solutions for 1x1x1 H8 element)
    nMat = uint16(size(matProp,1)); prop = reshape(matProp(1:nMat),1,1,nMat);
    z = zeros(1,1,nMat);
    a = 1/3 * prop; b = -1/12 * prop;
    K = [ a z b z z b b b ; z a z b b z b b ; b z a z b b z b ; z b z a b b b z ;
          z b b b a z b z ; b z b b z a z b ; b b z b b z a z ; b b b z z b z a ];
    a = 0.25 * prop;
    B = [ -a,  a, a, -a, -a,  a,  a, -a ; -a, -a, a,  a, -a, -a,  a,  a ; a,  a, a,  a, -a, -a, -a, -a ];
    clear a b z prop
    %% Jacobi preconditioner
    M = zeros(nDOFs,1,'double');
    g(:) = K(1,1,elemMatMap); M(DOFMap(n)) = M(DOFMap(n)) + g;
    n = n + nRows+1;
    g(:) = K(2,2,elemMatMap); M(DOFMap(n)) = M(DOFMap(n)) + g;
    n = n - 1;
    g(:) = K(3,3,elemMatMap); M(DOFMap(n)) = M(DOFMap(n)) + g;
    n = n - (nRows+1);
    g(:) = K(4,4,elemMatMap); M(DOFMap(n)) = M(DOFMap(n)) + g;
    n = n + (1+nNodes_xy);
    g(:) = K(5,5,elemMatMap); M(DOFMap(n)) = M(DOFMap(n)) + g;
    n = n + nRows+1;
    g(:) = K(6,6,elemMatMap); M(DOFMap(n)) = M(DOFMap(n)) + g;
    n = n - 1;
    g(:) = K(7,7,elemMatMap); M(DOFMap(n)) = M(DOFMap(n)) + g;
    n = n - (nRows+1);
    g(:) = K(8,8,elemMatMap); M(DOFMap(n)) = M(DOFMap(n)) + g;
    n = n - (nNodes_xy-1);
    M = M.^-1;
    %% Homogenization (ii=1 -> x, ii=2 -> y, ii=3 -> z)
    r = zeros(nDOFs,1,'double');
    x = zeros(nDOFs,1,'double');
    d = zeros(nDOFs,1,'double');
    q = zeros(nDOFs,1,'double');
    for ii=1:3
        %% Compute RHS - Domain
        r(:) = 0;
        g(:) = B(ii,1,elemMatMap); r(DOFMap(n)) = r(DOFMap(n)) + g;
        n = n + nRows+1;
        g(:) = B(ii,2,elemMatMap); r(DOFMap(n)) = r(DOFMap(n)) + g;
        n = n - 1;
        g(:) = B(ii,3,elemMatMap); r(DOFMap(n)) = r(DOFMap(n)) + g;
        n = n - (nRows+1);
        g(:) = B(ii,4,elemMatMap); r(DOFMap(n)) = r(DOFMap(n)) + g;
        n = n + (1+nNodes_xy);
        g(:) = B(ii,5,elemMatMap); r(DOFMap(n)) = r(DOFMap(n)) + g;
        n = n + nRows+1;
        g(:) = B(ii,6,elemMatMap); r(DOFMap(n)) = r(DOFMap(n)) + g;
        n = n - 1;
        g(:) = B(ii,7,elemMatMap); r(DOFMap(n)) = r(DOFMap(n)) + g;
        n = n - (nRows+1);
        g(:) = B(ii,8,elemMatMap); r(DOFMap(n)) = r(DOFMap(n)) + g;
        n = n - (nNodes_xy-1);
        %% Solve system (PCG - Elem by elem)
        x(:) = 0;                             % x0 = [0]
        d(:) = M.*r;                          % s  = M^-1 * r (d=s on first iteration)
        delta = dot(r,d); delta0 = delta; delta_prev = delta; % delta = dot(r,s)
        if (abs(delta)>10^-12) % check if initial guess did not already satisfy an absolute tolerance
            it = uint64(0);
            while (it < pcgIter), it = it+1;
                % q = K * d (lines commented out are associated with null coefficients of K)
                q(:) = 0;
                g(:) = K(1,1,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n));
               %g(:) = K(1,2,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nRows+1)));
                g(:) = K(1,3,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+nRows));
               %g(:) = K(1,4,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-1));
               %g(:) = K(1,5,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+nNodes_xy));
                g(:) = K(1,6,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nNodes_xy+nRows+1)));
                g(:) = K(1,7,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nNodes_xy+nRows)));
                g(:) = K(1,8,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nNodes_xy-1)));
                n = n+(nRows+1);
               %g(:) = K(2,1,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-(nRows+1)));
                g(:) = K(2,2,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n));
               %g(:) = K(2,3,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-1));
                g(:) = K(2,4,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-(nRows+2)));
                g(:) = K(2,5,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nNodes_xy-(nRows+1))));
               %g(:) = K(2,6,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+nNodes_xy));
                g(:) = K(2,7,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nNodes_xy-1)));
                g(:) = K(2,8,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nNodes_xy-(nRows+2))));
                n = n-1;
                g(:) = K(3,1,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nRows));
               %g(:) = K(3,2,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+1));
                g(:) = K(3,3,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n));
               %g(:) = K(3,4,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-(nRows+1)));
                g(:) = K(3,5,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nNodes_xy-nRows)));
                g(:) = K(3,6,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nNodes_xy+1)));
               %g(:) = K(3,7,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+nNodes_xy));
                g(:) = K(3,8,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nNodes_xy-(nRows+1))));
                n = n-(nRows+1);
               %g(:) = K(4,1,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+1));
                g(:) = K(4,2,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nRows+2)));
               %g(:) = K(4,3,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nRows+1)));
                g(:) = K(4,4,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n));
                g(:) = K(4,5,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nNodes_xy+1)));
                g(:) = K(4,6,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nNodes_xy+(nRows+2))));
                g(:) = K(4,7,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nNodes_xy+(nRows+1))));
               %g(:) = K(4,8,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+nNodes_xy));
                n = n+(1+nNodes_xy);
               %g(:) = K(5,1,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy));
                g(:) = K(5,2,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy+(nRows+1)));
                g(:) = K(5,3,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy+nRows));
                g(:) = K(5,4,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy-1));
                g(:) = K(5,5,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n));
               %g(:) = K(5,6,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nRows+1)));
                g(:) = K(5,7,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+nRows));
               %g(:) = K(5,8,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-1));
                n = n+(nRows+1);
                g(:) = K(6,1,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy-(nRows+1)));
               %g(:) = K(6,2,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy));
                g(:) = K(6,3,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy-1));
                g(:) = K(6,4,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy-(nRows+2)));
               %g(:) = K(6,5,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-(nRows+1)));
                g(:) = K(6,6,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n));
               %g(:) = K(6,7,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-1));
                g(:) = K(6,8,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-(nRows+2)));
                n = n-1;
                g(:) = K(7,1,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy-nRows));
                g(:) = K(7,2,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy+1));
               %g(:) = K(7,3,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy));
                g(:) = K(7,4,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy-(nRows+1)));
                g(:) = K(7,5,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nRows));
               %g(:) = K(7,6,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+1));
                g(:) = K(7,7,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n));
               %g(:) = K(7,8,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-(nRows+1)));
                n = n-(nRows+1);
                g(:) = K(8,1,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy+1));
                g(:) = K(8,2,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy+(nRows+2)));
                g(:) = K(8,3,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy+(nRows+1)));
               %g(:) = K(8,4,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nNodes_xy));
               %g(:) = K(8,5,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+1));
                g(:) = K(8,6,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nRows+2)));
               %g(:) = K(8,7,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nRows+1)));
                g(:) = K(8,8,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n));
                n = n - (nNodes_xy-1);
                a = delta / dot(d,q);         % a  = delta/dot(d,q)
                x = x + a*d;                  % x += a*d
                r = r - a*q;                  % r -= a*q
                q = M.*r;                     % s  = M^-1 * r (use q to store s)
                delta = dot(r,q);             % delta = dot(r,s)
                if (delta <= (delta0 * pcgTol^2)), break; end
                d = q + (delta/delta_prev)*d; % d = s + (delta/delta_prev)*d
                delta_prev = delta;
            end
            fprintf("PCG ran through %i steps.\nAdimensional residual: %e\n",it,sqrt(delta/delta0));
        else
            fprintf("Null solution satisfied PCG\n");
        end
        %% Update column ii of C
        qx = 0; qy = 0; qz = 0; t = zeros(8,1,'double'); t(([1,2,5,6]+ii)*(ii<3)+[1,2,3,4]*(ii>=3),:) = [1;1;1;1];
        g(:) = B(1,1,elemMatMap); qx = qx + sum(g.*(t(1)-x(DOFMap(n))));
        g(:) = B(2,1,elemMatMap); qy = qy + sum(g.*(t(1)-x(DOFMap(n))));
        g(:) = B(3,1,elemMatMap); qz = qz + sum(g.*(t(1)-x(DOFMap(n))));
        n = n+(nRows+1);
        g(:) = B(1,2,elemMatMap); qx = qx + sum(g.*(t(2)-x(DOFMap(n))));
        g(:) = B(2,2,elemMatMap); qy = qy + sum(g.*(t(2)-x(DOFMap(n))));
        g(:) = B(3,2,elemMatMap); qz = qz + sum(g.*(t(2)-x(DOFMap(n))));
        n = n-1;
        g(:) = B(1,3,elemMatMap); qx = qx + sum(g.*(t(3)-x(DOFMap(n))));
        g(:) = B(2,3,elemMatMap); qy = qy + sum(g.*(t(3)-x(DOFMap(n))));
        g(:) = B(3,3,elemMatMap); qz = qz + sum(g.*(t(3)-x(DOFMap(n))));
        n = n-(nRows+1);
        g(:) = B(1,4,elemMatMap); qx = qx + sum(g.*(t(4)-x(DOFMap(n))));
        g(:) = B(2,4,elemMatMap); qy = qy + sum(g.*(t(4)-x(DOFMap(n))));
        g(:) = B(3,4,elemMatMap); qz = qz + sum(g.*(t(4)-x(DOFMap(n))));
        n = n+(1+nNodes_xy);
        g(:) = B(1,5,elemMatMap); qx = qx + sum(g.*(t(5)-x(DOFMap(n))));
        g(:) = B(2,5,elemMatMap); qy = qy + sum(g.*(t(5)-x(DOFMap(n))));
        g(:) = B(3,5,elemMatMap); qz = qz + sum(g.*(t(5)-x(DOFMap(n))));
        n = n+(nRows+1);
        g(:) = B(1,6,elemMatMap); qx = qx + sum(g.*(t(6)-x(DOFMap(n))));
        g(:) = B(2,6,elemMatMap); qy = qy + sum(g.*(t(6)-x(DOFMap(n))));
        g(:) = B(3,6,elemMatMap); qz = qz + sum(g.*(t(6)-x(DOFMap(n))));
        n = n-1;
        g(:) = B(1,7,elemMatMap); qx = qx + sum(g.*(t(7)-x(DOFMap(n))));
        g(:) = B(2,7,elemMatMap); qy = qy + sum(g.*(t(7)-x(DOFMap(n))));
        g(:) = B(3,7,elemMatMap); qz = qz + sum(g.*(t(7)-x(DOFMap(n))));
        n = n-(nRows+1);
        g(:) = B(1,8,elemMatMap); qx = qx + sum(g.*(t(8)-x(DOFMap(n))));
        g(:) = B(2,8,elemMatMap); qy = qy + sum(g.*(t(8)-x(DOFMap(n))));
        g(:) = B(3,8,elemMatMap); qz = qz + sum(g.*(t(8)-x(DOFMap(n))));
        n = n - (nNodes_xy-1);
        C(:,ii) = [ qx ; qy ; qz ] / double(nElems);
    end
end
