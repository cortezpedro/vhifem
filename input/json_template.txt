{
    "type_of_analysis": 0, # 0 = thermal, 1 = elastic
    "solver_tolerance": 1e-06,
    "number_of_iterations": 1000,
    "image_dimensions": [
        50, # nRows
        50, # nCols
        50  # nLayers (if 0, analysis is 2D)
    ],
    "refinement": 1, # mesh refinement. 1 uses the original image
    "number_of_materials": 2,
    "properties_of_materials": [
        [
            0,      # 8-bit grayscale color
            129.0,  # thermal conductivity, or young's modulus
            0.25    # poisson coeff (only for elasticity)
        ],
        [
            # color of material 2
            # prop of material 2
        ]
    ]
}
