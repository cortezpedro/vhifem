%%-------------------------------------------------------------------------
% Vectorized Homogenization with Image-based FEM (Elasticity_3D)
% Input:
% * dim        = [n_rows,n_cols,n_layers]
% * elemMatMap = array with material keys (this is the image)
% * matProp    = array with the isotropic elasticity coeffs [E,v] of each phase
% * pcgTol     = dimensionless tolerance for the PCG method
% * pcgIter    = max number of iterations for the PCG method
% Output:
% * C = matrix representation of the homogenized constitutive tensor
%%-------------------------------------------------------------------------
function C = vhifem_Elastic_3D(dim,elemMatMap,matProp,pcgTol,pcgIter)
    C = zeros(6,6);
    %% Generate DOF map
    nElems = uint64(length(elemMatMap)); nDOFs = uint64(3*nElems);
    nRows = dim(1); nCols = dim(2); nNodes_xy = (nRows+1)*(nCols+1);
    DOFMap = zeros(nRows+1,nCols+1,dim(3)+1,'uint64');
    DOFMap(1:nRows,1:nCols,1:dim(3)) = reshape(1:(nDOFs/3),nRows,nCols,dim(3));
    DOFMap(nRows+1,:,:) = DOFMap(1,:,:); DOFMap(:,nCols+1,:) = DOFMap(:,1,:); DOFMap(:,:,dim(3)+1) = DOFMap(:,:,1);
    DOFMap = 3*DOFMap(:)-2;
    %% Assemble elem arrays
    ee = 1:nElems; n = mod(ee-1,nCols*nRows);
    n  = 2+n+uint64(floor(double(n)/double(nRows)))+uint64(floor(double(ee-1)/double(nCols*nRows)))*(nNodes_xy);
    g  = zeros(nElems,1,'double');
    clear ee
    %% Compute local matrices (analytical solutions for 1x1x1 H8 element)
    nMat = uint16(size(matProp,1));
    E = reshape(matProp(1:nMat,1),1,1,nMat);
    v = reshape(matProp(1:nMat,2),1,1,nMat);
    c_1 = (E.*(4*v - 1))./(24*(2*v - 1).*(v + 1));
    c_2 = c_1.*0.5;
    c_3 = E./(18*(2*v - 1).*(v + 1));
    c_4 = c_3.*0.75;
    c_5 = c_3.*0.5;
    c_6 = c_4.*0.5;
    c_7 = (E.*(3*v - 2))./( 9*(2*v - 1).*(v + 1));
    c_8 = (E.*(3*v - 1))./(36*(2*v - 1).*(v + 1));
    c_9 = c_7.*0.25;
    c_10 = (E.*(6*v - 5))./(72*(2*v - 1).*(v + 1));
    c_11 = E./(36*(2*v - 1)) + (5*E)./(72*(v + 1));
    c_12 = E./(72*(2*v - 1)) + (5*E)./(144*(v + 1));
    K = [  c_7,-c_4,c_4,c_3,-c_1, c_1,-c_10,c_4, c_2,-c_5,c_1, c_6,-c_5,-c_6,-c_1,-c_10,-c_2,-c_4,-c_9, c_6,-c_6,-c_8, c_2, -c_2;
          -c_4,c_7,c_4,c_1,-c_5, c_6,c_4,-c_10, c_2,-c_1,c_3, c_1,-c_6,-c_5,-c_1, c_2,-c_8, -c_2, c_6,-c_9,-c_6,-c_2,-c_10,-c_4;
           c_4, c_4,c_7,-c_1, c_6,-c_5,-c_2,-c_2,-c_8, c_6,-c_1,-c_5,c_11,c_11, c_3,-c_4, c_12,-c_10,-c_6,-c_6,-c_9, c_12,-c_4,-c_10;
           c_3, c_1,-c_1,c_7, c_4,-c_4,-c_5,-c_1,-c_6,-c_10,-c_4,-c_2,-c_10,c_2, c_4,-c_5,c_6,c_11,-c_8,-c_2, c_12,-c_9,-c_6, c_6;
          -c_1,-c_5,c_6,c_4,c_7, c_4,c_1,c_3, c_1,-c_4,-c_10, c_2,-c_2,-c_8, -c_2, c_6,-c_5,-c_1, c_2,-c_10,-c_4,-c_6,-c_9,-c_6;
           c_1, c_6,-c_5,-c_4, c_4,c_7,-c_6,-c_1,-c_5, c_2,-c_2,-c_8,c_4, c_12,-c_10,-c_1,c_11, c_3,-c_2,-c_4,-c_10,c_6,-c_6,-c_9;
          -c_10, c_4,-c_2,-c_5, c_1,-c_6,c_7,-c_4,-c_4, c_3,-c_1,-c_1,-c_9,c_6, c_6,-c_8,c_2,c_12,-c_5,-c_6,c_11,-c_10,-c_2, c_4;
           c_4,-c_10,-c_2,-c_1, c_3,-c_1,-c_4,c_7,-c_4, c_1,-c_5,-c_6,c_6,-c_9, c_6,-c_2,-c_10, c_4,-c_6,-c_5,c_11,c_2,-c_8,c_12;
           c_2, c_2,-c_8,-c_6, c_1,-c_5,-c_4,-c_4,c_7, c_1,-c_6,-c_5,c_6,c_6,-c_9, -c_2,c_4,-c_10,-c_1,-c_1,c_3,c_4, -c_2,-c_10;
          -c_5,-c_1,c_6,-c_10,-c_4, c_2,c_3,c_1, c_1,c_7,c_4, c_4,-c_8,-c_2, -c_2,-c_9,-c_6,-c_6,-c_10, c_2,-c_4,-c_5, c_6,-c_1;
           c_1, c_3,-c_1,-c_4,-c_10,-c_2,-c_1,-c_5,-c_6, c_4,c_7,-c_4,c_2,-c_10, c_4,-c_6,-c_9, c_6,-c_2,-c_8, c_12,c_6,-c_5,c_11;
           c_6, c_1,-c_5,-c_2, c_2,-c_8,-c_1,-c_6,-c_5, c_4,-c_4,c_7, c_12,c_4,-c_10,-c_6,c_6,-c_9,-c_4, -c_2,-c_10,c_11,-c_1, c_3;
          -c_5,-c_6,c_11,-c_10,-c_2, c_4,-c_9,c_6, c_6,-c_8,c_2,c_12,c_7,-c_4,-c_4, c_3,-c_1,-c_1,-c_10, c_4,-c_2,-c_5, c_1,-c_6;
          -c_6,-c_5,c_11,c_2,-c_8,c_12,c_6,-c_9, c_6,-c_2,-c_10, c_4,-c_4,c_7,-c_4, c_1,-c_5,-c_6, c_4,-c_10,-c_2,-c_1, c_3,-c_1;
          -c_1,-c_1,c_3,c_4, -c_2,-c_10,c_6,c_6,-c_9, -c_2,c_4,-c_10,-c_4,-c_4,c_7, c_1,-c_6,-c_5, c_2, c_2,-c_8,-c_6, c_1,-c_5;
          -c_10, c_2,-c_4,-c_5, c_6,-c_1,-c_8,-c_2, -c_2,-c_9,-c_6,-c_6,c_3,c_1, c_1,c_7,c_4, c_4,-c_5,-c_1,c_6,-c_10,-c_4, c_2;
          -c_2,-c_8, c_12,c_6,-c_5,c_11,c_2,-c_10, c_4,-c_6,-c_9, c_6,-c_1,-c_5,-c_6, c_4,c_7,-c_4, c_1, c_3,-c_1,-c_4,-c_10,-c_2;
          -c_4, -c_2,-c_10,c_11,-c_1, c_3, c_12,c_4,-c_10,-c_6,c_6,-c_9,-c_1,-c_6,-c_5, c_4,-c_4,c_7, c_6, c_1,-c_5,-c_2, c_2,-c_8;
          -c_9, c_6,-c_6,-c_8, c_2, -c_2,-c_5,-c_6,-c_1,-c_10,-c_2,-c_4,-c_10,c_4, c_2,-c_5,c_1, c_6,c_7,-c_4,c_4,c_3,-c_1, c_1;
           c_6,-c_9,-c_6,-c_2,-c_10,-c_4,-c_6,-c_5,-c_1, c_2,-c_8, -c_2,c_4,-c_10, c_2,-c_1,c_3, c_1,-c_4,c_7,c_4,c_1,-c_5, c_6;
          -c_6,-c_6,-c_9, c_12,-c_4,-c_10,c_11,c_11, c_3,-c_4, c_12,-c_10,-c_2,-c_2,-c_8, c_6,-c_1,-c_5, c_4, c_4,c_7,-c_1, c_6,-c_5;
          -c_8,-c_2, c_12,-c_9,-c_6, c_6,-c_10,c_2, c_4,-c_5,c_6,c_11,-c_5,-c_1,-c_6,-c_10,-c_4,-c_2, c_3, c_1,-c_1,c_7, c_4,-c_4;
           c_2,-c_10,-c_4,-c_6,-c_9,-c_6,-c_2,-c_8, -c_2, c_6,-c_5,-c_1,c_1,c_3, c_1,-c_4,-c_10, c_2,-c_1,-c_5,c_6,c_4,c_7, c_4;
          -c_2,-c_4,-c_10,c_6,-c_6,-c_9,c_4, c_12,-c_10,-c_1,c_11, c_3,-c_6,-c_1,-c_5, c_2,-c_2,-c_8, c_1, c_6,-c_5,-c_4, c_4,c_7 ];
    c_1=E./(8*(v+1));
    c_2=(E.*(v-1))./(4*(2*v-1).*(v+1));
    c_3=(E.*v)./(4*(2*v-1).*(v+1));
    z = zeros(1,1,nMat);
    B = [ -c_2,c_3,-c_3,c_2,c_3,-c_3,c_2,-c_3,-c_3,-c_2,-c_3,-c_3,-c_2,c_3,c_3,c_2,c_3,c_3,c_2,-c_3,c_3,-c_2,-c_3,c_3;
           c_3,-c_2,-c_3,-c_3,-c_2,-c_3,-c_3,c_2,-c_3,c_3,c_2,-c_3,c_3,-c_2,c_3,-c_3,-c_2,c_3,-c_3,c_2,c_3,c_3,c_2,c_3;
           c_3,c_3,c_2,-c_3,c_3,c_2,-c_3,-c_3,c_2,c_3,-c_3,c_2,c_3,c_3,-c_2,-c_3,c_3,-c_2,-c_3,-c_3,-c_2,c_3,-c_3,-c_2;
           z,c_1,-c_1,z,c_1,-c_1,z,c_1,c_1,z,c_1,c_1,z,-c_1,-c_1,z,-c_1,-c_1,z,-c_1,c_1,z,-c_1,c_1;
           c_1,z,-c_1,c_1,z,c_1,c_1,z,c_1,c_1,z,-c_1,-c_1,z,-c_1,-c_1,z,c_1,-c_1,z,c_1,-c_1,z,-c_1;
          -c_1,-c_1,z,-c_1,c_1,z,c_1,c_1,z,c_1,-c_1,z,-c_1,-c_1,z,-c_1,c_1,z,c_1,c_1,z,c_1,-c_1,z ];
    clear c_1 c_2 c_3 c_4 c_5 c_6 c_7 c_8 c_9 c_10 c_11 c12 z E v
    %% Jacobi preconditioner
    M = zeros(nDOFs,1,'double');
    g(:) = K(1,1,elemMatMap); M(DOFMap(n))   = M(DOFMap(n))   + g;
    g(:) = K(2,2,elemMatMap); M(DOFMap(n)+1) = M(DOFMap(n)+1) + g;
    g(:) = K(3,3,elemMatMap); M(DOFMap(n)+2) = M(DOFMap(n)+2) + g;
    n = n + (nRows+1);
    g(:) = K(4,4,elemMatMap); M(DOFMap(n))   = M(DOFMap(n))   + g;
    g(:) = K(5,5,elemMatMap); M(DOFMap(n)+1) = M(DOFMap(n)+1) + g;
    g(:) = K(6,6,elemMatMap); M(DOFMap(n)+2) = M(DOFMap(n)+2) + g;
    n = n - 1;
    g(:) = K(7,7,elemMatMap); M(DOFMap(n))   = M(DOFMap(n))   + g;
    g(:) = K(8,8,elemMatMap); M(DOFMap(n)+1) = M(DOFMap(n)+1) + g;
    g(:) = K(9,9,elemMatMap); M(DOFMap(n)+2) = M(DOFMap(n)+2) + g;
    n = n - (nRows+1);
    g(:) = K(10,10,elemMatMap); M(DOFMap(n))   = M(DOFMap(n))   + g;
    g(:) = K(11,11,elemMatMap); M(DOFMap(n)+1) = M(DOFMap(n)+1) + g;
    g(:) = K(12,12,elemMatMap); M(DOFMap(n)+2) = M(DOFMap(n)+2) + g;
    n = n +(1+nNodes_xy);
    g(:) = K(13,13,elemMatMap); M(DOFMap(n))   = M(DOFMap(n))   + g;
    g(:) = K(14,14,elemMatMap); M(DOFMap(n)+1) = M(DOFMap(n)+1) + g;
    g(:) = K(15,15,elemMatMap); M(DOFMap(n)+2) = M(DOFMap(n)+2) + g;
    n = n + (nRows+1);
    g(:) = K(16,16,elemMatMap); M(DOFMap(n))   = M(DOFMap(n))   + g;
    g(:) = K(17,17,elemMatMap); M(DOFMap(n)+1) = M(DOFMap(n)+1) + g;
    g(:) = K(18,18,elemMatMap); M(DOFMap(n)+2) = M(DOFMap(n)+2) + g;
    n = n - 1;
    g(:) = K(19,19,elemMatMap); M(DOFMap(n))   = M(DOFMap(n))   + g;
    g(:) = K(20,20,elemMatMap); M(DOFMap(n)+1) = M(DOFMap(n)+1) + g;
    g(:) = K(21,21,elemMatMap); M(DOFMap(n)+2) = M(DOFMap(n)+2) + g;
    n = n - (nRows+1);
    g(:) = K(22,22,elemMatMap); M(DOFMap(n))   = M(DOFMap(n))   + g;
    g(:) = K(23,23,elemMatMap); M(DOFMap(n)+1) = M(DOFMap(n)+1) + g;
    g(:) = K(24,24,elemMatMap); M(DOFMap(n)+2) = M(DOFMap(n)+2) + g;
    n = n -(nNodes_xy-1);
    M = M.^-1;
    %% Homogenization (ii=1 -> x, ii=2 -> y, ii=3 -> z, ii=4 -> yz(shear), ii=5 -> xz(shear), ii=6 -> xy(shear))
    r = zeros(nDOFs,1,'double');
    x = zeros(nDOFs,1,'double');
    d = zeros(nDOFs,1,'double');
    q = zeros(nDOFs,1,'double');
    for ii=1:6
        %% Compute RHS - Domain
        g(:) = B(ii,1,elemMatMap); r(DOFMap(n))   = r(DOFMap(n))   + g;
        g(:) = B(ii,2,elemMatMap); r(DOFMap(n)+1) = r(DOFMap(n)+1) + g;
        g(:) = B(ii,3,elemMatMap); r(DOFMap(n)+2) = r(DOFMap(n)+2) + g;
        n = n + (nRows+1);
        g(:) = B(ii,4,elemMatMap); r(DOFMap(n))   = r(DOFMap(n))   + g;
        g(:) = B(ii,5,elemMatMap); r(DOFMap(n)+1) = r(DOFMap(n)+1) + g;
        g(:) = B(ii,6,elemMatMap); r(DOFMap(n)+2) = r(DOFMap(n)+2) + g;
        n = n - 1;
        g(:) = B(ii,7,elemMatMap); r(DOFMap(n))   = r(DOFMap(n))   + g;
        g(:) = B(ii,8,elemMatMap); r(DOFMap(n)+1) = r(DOFMap(n)+1) + g;
        g(:) = B(ii,9,elemMatMap); r(DOFMap(n)+2) = r(DOFMap(n)+2) + g;
        n = n - (nRows+1);
        g(:) = B(ii,10,elemMatMap); r(DOFMap(n))   = r(DOFMap(n))   + g;
        g(:) = B(ii,11,elemMatMap); r(DOFMap(n)+1) = r(DOFMap(n)+1) + g;
        g(:) = B(ii,12,elemMatMap); r(DOFMap(n)+2) = r(DOFMap(n)+2) + g;
        n = n +(1+nNodes_xy);
        g(:) = B(ii,13,elemMatMap); r(DOFMap(n))   = r(DOFMap(n))   + g;
        g(:) = B(ii,14,elemMatMap); r(DOFMap(n)+1) = r(DOFMap(n)+1) + g;
        g(:) = B(ii,15,elemMatMap); r(DOFMap(n)+2) = r(DOFMap(n)+2) + g;
        n = n + (nRows+1);
        g(:) = B(ii,16,elemMatMap); r(DOFMap(n))   = r(DOFMap(n))   + g;
        g(:) = B(ii,17,elemMatMap); r(DOFMap(n)+1) = r(DOFMap(n)+1) + g;
        g(:) = B(ii,18,elemMatMap); r(DOFMap(n)+2) = r(DOFMap(n)+2) + g;
        n = n - 1;
        g(:) = B(ii,19,elemMatMap); r(DOFMap(n))   = r(DOFMap(n))   + g;
        g(:) = B(ii,20,elemMatMap); r(DOFMap(n)+1) = r(DOFMap(n)+1) + g;
        g(:) = B(ii,21,elemMatMap); r(DOFMap(n)+2) = r(DOFMap(n)+2) + g;
        n = n - (nRows+1);
        g(:) = B(ii,22,elemMatMap); r(DOFMap(n))   = r(DOFMap(n))   + g;
        g(:) = B(ii,23,elemMatMap); r(DOFMap(n)+1) = r(DOFMap(n)+1) + g;
        g(:) = B(ii,24,elemMatMap); r(DOFMap(n)+2) = r(DOFMap(n)+2) + g;
        n = n -(nNodes_xy-1);
        %% Solve system (PCG - Elem by elem)
        x(:) = 0;                             % x0 = [0]
        d(:) = M.*r;                          % s  = M^-1 * r (d=s on first iteration)
        delta = dot(r,d); delta0 = delta; delta_prev = delta; % delta = dot(r,s)
        if (abs(delta)>10^-12) % check if initial guess did not already satisfy an absolute tolerance
            it = uint64(0);
            while (it < pcgIter), it = it+1;
                % q = K * d
                q(:) = 0;
                % Constant dimension loop through the DOFs of each node
                line = uint8(1);
                for jj = 0:2
                    g(:) = K(line,1,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n));
                    g(:) = K(line,2,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+1);
                    g(:) = K(line,3,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+2);
                    g(:) = K(line,4,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+1)));
                    g(:) = K(line,5,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+1))+1);
                    g(:) = K(line,6,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+1))+2);
                    g(:) = K(line,7,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nRows));
                    g(:) = K(line,8,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nRows)+1);
                    g(:) = K(line,9,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nRows)+2);
                    g(:) = K(line,10,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-1));
                    g(:) = K(line,11,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-1)+1);
                    g(:) = K(line,12,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-1)+2);
                    g(:) = K(line,13,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nNodes_xy));
                    g(:) = K(line,14,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nNodes_xy)+1);
                    g(:) = K(line,15,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nNodes_xy)+2);
                    g(:) = K(line,16,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+nRows+1)));
                    g(:) = K(line,17,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+nRows+1))+1);
                    g(:) = K(line,18,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+nRows+1))+2);
                    g(:) = K(line,19,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+nRows)));
                    g(:) = K(line,20,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+nRows))+1);
                    g(:) = K(line,21,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+nRows))+2);
                    g(:) = K(line,22,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-1)));
                    g(:) = K(line,23,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-1))+1);
                    g(:) = K(line,24,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-1))+2);
                    line = line + 3;
                    n = n+(nRows+1);
                    g(:) = K(line,1,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+1)));
                    g(:) = K(line,2,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+1))+1);
                    g(:) = K(line,3,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+1))+2);
                    g(:) = K(line,4,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n));
                    g(:) = K(line,5,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+1);
                    g(:) = K(line,6,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+2);
                    g(:) = K(line,7,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-1));
                    g(:) = K(line,8,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-1)+1);
                    g(:) = K(line,9,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-1)+2);
                    g(:) = K(line,10,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+2)));
                    g(:) = K(line,11,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+2))+1);
                    g(:) = K(line,12,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+2))+2);
                    g(:) = K(line,13,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-nRows-1)));
                    g(:) = K(line,14,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-nRows-1))+1);
                    g(:) = K(line,15,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-nRows-1))+2);
                    g(:) = K(line,16,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nNodes_xy));
                    g(:) = K(line,17,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nNodes_xy)+1);
                    g(:) = K(line,18,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nNodes_xy)+2);
                    g(:) = K(line,19,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-1)));
                    g(:) = K(line,20,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-1))+1);
                    g(:) = K(line,21,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-1))+2);
                    g(:) = K(line,22,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-nRows-2)));
                    g(:) = K(line,23,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-nRows-2))+1);
                    g(:) = K(line,24,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-nRows-2))+2);
                    line = line + 3;
                    n = n-1;
                    g(:) = K(line,1,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nRows));
                    g(:) = K(line,2,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nRows)+1);
                    g(:) = K(line,3,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nRows)+2);
                    g(:) = K(line,4,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+1));
                    g(:) = K(line,5,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+1)+1);
                    g(:) = K(line,6,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+1)+2);
                    g(:) = K(line,7,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n));
                    g(:) = K(line,8,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+1);
                    g(:) = K(line,9,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+2);
                    g(:) = K(line,10,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+1)));
                    g(:) = K(line,11,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+1))+1);
                    g(:) = K(line,12,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+1))+2);
                    g(:) = K(line,13,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-nRows)));
                    g(:) = K(line,14,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-nRows))+1);
                    g(:) = K(line,15,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-nRows))+2);
                    g(:) = K(line,16,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+1)));
                    g(:) = K(line,17,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+1))+1);
                    g(:) = K(line,18,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+1))+2);
                    g(:) = K(line,19,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nNodes_xy));
                    g(:) = K(line,20,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nNodes_xy)+1);
                    g(:) = K(line,21,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nNodes_xy)+2);
                    g(:) = K(line,22,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-nRows-1)));
                    g(:) = K(line,23,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-nRows-1))+1);
                    g(:) = K(line,24,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy-nRows-1))+2);
                    line = line + 3;
                    n = n-(nRows+1);
                    g(:) = K(line,1,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+1));
                    g(:) = K(line,2,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+1)+1);
                    g(:) = K(line,3,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+1)+2);
                    g(:) = K(line,4,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+2)));
                    g(:) = K(line,5,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+2))+1);
                    g(:) = K(line,6,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+2))+2);
                    g(:) = K(line,7,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+1)));
                    g(:) = K(line,8,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+1))+1);
                    g(:) = K(line,9,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+1))+2);
                    g(:) = K(line,10,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n));
                    g(:) = K(line,11,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+1);
                    g(:) = K(line,12,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+2);
                    g(:) = K(line,13,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+1)));
                    g(:) = K(line,14,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+1))+1);
                    g(:) = K(line,15,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+1))+2);
                    g(:) = K(line,16,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+nRows+2)));
                    g(:) = K(line,17,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+nRows+2))+1);
                    g(:) = K(line,18,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+nRows+2))+2);
                    g(:) = K(line,19,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+nRows+1)));
                    g(:) = K(line,20,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+nRows+1))+1);
                    g(:) = K(line,21,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nNodes_xy+nRows+1))+2);
                    g(:) = K(line,22,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nNodes_xy));
                    g(:) = K(line,23,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nNodes_xy)+1);
                    g(:) = K(line,24,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nNodes_xy)+2);
                    line = line + 3;
                    n = n+(1+nNodes_xy);
                    g(:) = K(line,1,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nNodes_xy));
                    g(:) = K(line,2,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nNodes_xy)+1);
                    g(:) = K(line,3,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nNodes_xy)+2);
                    g(:) = K(line,4,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-nRows-1)));
                    g(:) = K(line,5,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-nRows-1))+1);
                    g(:) = K(line,6,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-nRows-1))+2);
                    g(:) = K(line,7,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-nRows)));
                    g(:) = K(line,8,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-nRows))+1);
                    g(:) = K(line,9,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-nRows))+2);
                    g(:) = K(line,10,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+1)));
                    g(:) = K(line,11,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+1))+1);
                    g(:) = K(line,12,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+1))+2);
                    g(:) = K(line,13,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n));
                    g(:) = K(line,14,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+1);
                    g(:) = K(line,15,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+2);
                    g(:) = K(line,16,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+1)));
                    g(:) = K(line,17,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+1))+1);
                    g(:) = K(line,18,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+1))+2);
                    g(:) = K(line,19,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nRows));
                    g(:) = K(line,20,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nRows)+1);
                    g(:) = K(line,21,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+nRows)+2);
                    g(:) = K(line,22,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-1));
                    g(:) = K(line,23,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-1)+1);
                    g(:) = K(line,24,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-1)+2);
                    line = line + 3;
                    n = n+(nRows+1);
                    g(:) = K(line,1,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+nRows+1)));
                    g(:) = K(line,2,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+nRows+1))+1);
                    g(:) = K(line,3,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+nRows+1))+2);
                    g(:) = K(line,4,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nNodes_xy));
                    g(:) = K(line,5,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nNodes_xy)+1);
                    g(:) = K(line,6,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nNodes_xy)+2);
                    g(:) = K(line,7,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+1)));
                    g(:) = K(line,8,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+1))+1);
                    g(:) = K(line,9,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+1))+2);
                    g(:) = K(line,10,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+nRows+2)));
                    g(:) = K(line,11,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+nRows+2))+1);
                    g(:) = K(line,12,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+nRows+2))+2);
                    g(:) = K(line,13,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+1)));
                    g(:) = K(line,14,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+1))+1);
                    g(:) = K(line,15,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+1))+2);
                    g(:) = K(line,16,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n));
                    g(:) = K(line,17,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+1);
                    g(:) = K(line,18,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+2);
                    g(:) = K(line,19,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-1));
                    g(:) = K(line,20,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-1)+1);
                    g(:) = K(line,21,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-1)+2);
                    g(:) = K(line,22,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+2)));
                    g(:) = K(line,23,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+2))+1);
                    g(:) = K(line,24,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+2))+2);
                    line = line + 3;
                    n = n-1;
                    g(:) = K(line,1,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+nRows)));
                    g(:) = K(line,2,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+nRows))+1);
                    g(:) = K(line,3,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+nRows))+2);
                    g(:) = K(line,4,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-1)));
                    g(:) = K(line,5,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-1))+1);
                    g(:) = K(line,6,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-1))+2);
                    g(:) = K(line,7,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nNodes_xy));
                    g(:) = K(line,8,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nNodes_xy)+1);
                    g(:) = K(line,9,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nNodes_xy)+2);
                    g(:) = K(line,10,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+nRows+1)));
                    g(:) = K(line,11,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+nRows+1))+1);
                    g(:) = K(line,12,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy+nRows+1))+2);
                    g(:) = K(line,13,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nRows));
                    g(:) = K(line,14,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nRows)+1);
                    g(:) = K(line,15,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nRows)+2);
                    g(:) = K(line,16,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+1));
                    g(:) = K(line,17,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+1)+1);
                    g(:) = K(line,18,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+1)+2);
                    g(:) = K(line,19,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n));
                    g(:) = K(line,20,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+1);
                    g(:) = K(line,21,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+2);
                    g(:) = K(line,22,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+1)));
                    g(:) = K(line,23,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+1))+1);
                    g(:) = K(line,24,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nRows+1))+2);
                    line = line + 3;
                    n = n-(nRows+1);
                    g(:) = K(line,1,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-1)));
                    g(:) = K(line,2,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-1))+1);
                    g(:) = K(line,3,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-1))+2);
                    g(:) = K(line,4,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-nRows-2)));
                    g(:) = K(line,5,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-nRows-2))+1);
                    g(:) = K(line,6,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-nRows-2))+2);
                    g(:) = K(line,7,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-nRows-1)));
                    g(:) = K(line,8,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-nRows-1))+1);
                    g(:) = K(line,9,elemMatMap);  q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-(nNodes_xy-nRows-1))+2);
                    g(:) = K(line,10,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nNodes_xy));
                    g(:) = K(line,11,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nNodes_xy)+1);
                    g(:) = K(line,12,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n-nNodes_xy)+2);
                    g(:) = K(line,13,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+1));
                    g(:) = K(line,14,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+1)+1);
                    g(:) = K(line,15,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+1)+2);
                    g(:) = K(line,16,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+2)));
                    g(:) = K(line,17,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+2))+1);
                    g(:) = K(line,18,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+2))+2);
                    g(:) = K(line,19,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+1)));
                    g(:) = K(line,20,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+1))+1);
                    g(:) = K(line,21,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n+(nRows+1))+2);
                    g(:) = K(line,22,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n));
                    g(:) = K(line,23,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+1);
                    g(:) = K(line,24,elemMatMap); q(DOFMap(n)+jj) = q(DOFMap(n)+jj) + g.*d(DOFMap(n)+2);
                    line = line - 20;
                    n = n -(nNodes_xy-1);
                end
                a = delta / dot(d,q);         % a  = delta/dot(d,q)
                x = x + a*d;                  % x += a*d
                r = r - a*q;                  % r -= a*q
                q = M.*r;                     % s  = M^-1 * r (use q to store s)
                delta = dot(r,q);             % delta = dot(r,s)
                if (delta <= (delta0 * pcgTol^2)), break; end
                d = q + (delta/delta_prev)*d; % d = s + (delta/delta_prev)*d
                delta_prev = delta;
            end
            fprintf("PCG ran through %i steps.\nAdimensional residual: %e\n",it,sqrt(delta/delta0));
        else
            fprintf("Null solution satisfied PCG\n");
        end
        %% Update column jj of C
        qx = 0; qy = 0; qz = 0; qyz = 0; qxz = 0; qxy = 0; displ = zeros(24,1,'double');
        displ([4,7,16,19]*(ii==1)+[8,11,20,23]*(ii==2)+[3,6,9,12]*(ii==3)+[2,5,8,11]*(ii==4)+[1,4,7,10]*(ii==5)+[7,10,19,22]*(ii==6),:) = [1;1;1;1];
        row = uint8(1);
        for jj = 0:2
            g(:) = B(1,row,elemMatMap); qx  = qx  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(2,row,elemMatMap); qy  = qy  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(3,row,elemMatMap); qz  = qz  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(4,row,elemMatMap); qyz = qyz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(5,row,elemMatMap); qxz = qxz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(6,row,elemMatMap); qxy = qxy + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            row = row + 3;
            n = n+(nRows+1);
            g(:) = B(1,row,elemMatMap); qx  = qx  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(2,row,elemMatMap); qy  = qy  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(3,row,elemMatMap); qz  = qz  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(4,row,elemMatMap); qyz = qyz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(5,row,elemMatMap); qxz = qxz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(6,row,elemMatMap); qxy = qxy + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            row = row + 3;
            n = n-1;
            g(:) = B(1,row,elemMatMap); qx  = qx  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(2,row,elemMatMap); qy  = qy  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(3,row,elemMatMap); qz  = qz  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(4,row,elemMatMap); qyz = qyz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(5,row,elemMatMap); qxz = qxz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(6,row,elemMatMap); qxy = qxy + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            row = row + 3;
            n = n-(nRows+1);
            g(:) = B(1,row,elemMatMap); qx  = qx  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(2,row,elemMatMap); qy  = qy  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(3,row,elemMatMap); qz  = qz  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(4,row,elemMatMap); qyz = qyz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(5,row,elemMatMap); qxz = qxz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(6,row,elemMatMap); qxy = qxy + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            row = row + 3;
            n = n+(1+nNodes_xy);
            g(:) = B(1,row,elemMatMap); qx  = qx  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(2,row,elemMatMap); qy  = qy  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(3,row,elemMatMap); qz  = qz  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(4,row,elemMatMap); qyz = qyz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(5,row,elemMatMap); qxz = qxz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(6,row,elemMatMap); qxy = qxy + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            row = row + 3;
            n = n+(nRows+1);
            g(:) = B(1,row,elemMatMap); qx  = qx  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(2,row,elemMatMap); qy  = qy  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(3,row,elemMatMap); qz  = qz  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(4,row,elemMatMap); qyz = qyz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(5,row,elemMatMap); qxz = qxz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(6,row,elemMatMap); qxy = qxy + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            row = row + 3;
            n = n-1;
            g(:) = B(1,row,elemMatMap); qx  = qx  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(2,row,elemMatMap); qy  = qy  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(3,row,elemMatMap); qz  = qz  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(4,row,elemMatMap); qyz = qyz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(5,row,elemMatMap); qxz = qxz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(6,row,elemMatMap); qxy = qxy + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            row = row + 3;
            n = n-(nRows+1);
            g(:) = B(1,row,elemMatMap); qx  = qx  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(2,row,elemMatMap); qy  = qy  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(3,row,elemMatMap); qz  = qz  + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(4,row,elemMatMap); qyz = qyz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(5,row,elemMatMap); qxz = qxz + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            g(:) = B(6,row,elemMatMap); qxy = qxy + sum(g.*(displ(row)-x(DOFMap(n)+jj)));
            row = row - 20;
            n = n -(nNodes_xy-1);
        end
        C(:,ii) = [ qx ; qy ; qz; qyz; qxz; qxy ] / double(nElems);
    end    
end
