%% Main function
function C = vhifem(json_file,materialmap_file)
    C = double.empty;
    % Check input
    if (nargin == 1)
        materialmap_file = strcat(json_file,".raw");
        json_file = strcat(json_file,".json");
    end
    % Read JSON file
    data = readJSON(json_file); if isempty(data), return; end
    % Read RAW or Tiff file and assemble elemMatMap
    elemMatMap = readElemMatMap(materialmap_file,data); if isempty(elemMatMap), return; end
    % Solve homogenization
    tic();
    if data.analysis == 0 % thermal
        if data.dim(3) % 3D
            C = vhifem_Thermal_3D(data.dim,elemMatMap,data.matProp,data.pcgTol,data.pcgIter);
        else % 2D
            C = vhifem_Thermal_2D(data.dim,elemMatMap,data.matProp,data.pcgTol,data.pcgIter);
        end
    else % if data.analysis == 1 % elastic
        if data.dim(3) % 3D
            C = vhifem_Elastic_3D(data.dim,elemMatMap,data.matProp,data.pcgTol,data.pcgIter);
        else % 2D
            C = vhifem_Elastic_2D(data.dim,elemMatMap,data.matProp,data.pcgTol,data.pcgIter);
        end
    end
    toc();
end
%% JSON file reader
function model_data = readJSON(filepath)
    model_data = [];
    try data = jsondecode(fileread(filepath)); catch, fprintf("ERROR: Invalid JSON file.\n"); return; end
    % Get type of analysis
    if (isfield(data,"type_of_analysis"))
        type_of_analysis = uint8(data.type_of_analysis);
        if (type_of_analysis ~= 0 &&... % thermal
            type_of_analysis ~= 1)      % elastic
            return;
        end
    end
    % Get image dim
    if (~isfield(data,"image_dimensions")), return; end
    nx = uint64(data.image_dimensions(1));
    ny = uint64(data.image_dimensions(2));
    nz = uint64(data.image_dimensions(3));
    is3D = false;
    if (nz > 0), is3D = true; end
    % Get number of materials
    if (~isfield(data,"number_of_materials")), return; end
    nMat = uint16(data.number_of_materials);
    % Get materials properties
    if (~isfield(data,"properties_of_materials")), return; end
    materials = double(data.properties_of_materials);
    matKeys = zeros(256,1,'uint16');
    if (type_of_analysis == 0) % thermal
        matProp = zeros(nMat,1,'double');
        for i=1:nMat
            matKeys(uint16(materials(i,1)+1)) = uint16(i);
            matProp(i) = double(materials(i,2));
        end
    else % if (type_of_analysis == 1) % elastic
        matProp = zeros(nMat,2,'double');
        for i=1:nMat
            matKeys(uint16(materials(i,1)+1)) = uint16(i);
            matProp(i,1) = double(materials(i,2));
            matProp(i,2) = double(materials(i,3));
        end
    end
    % Get refinement
    refinement = uint64(1);
    if (isfield(data,"refinement"))
        refinement = uint64(data.refinement);
    end
    % Get solver tolerance
    pcgTol = double(10^-6);
    if (isfield(data,"solver_tolerance"))
        pcgTol = double(data.solver_tolerance);
    end
    % Get solver max number of steps
    pcgIter = nx*ny*refinement*refinement;
    if (is3D), pcgIter = pcgIter*nz*refinement; end
    if (isfield(data,"number_of_iterations"))
        pcgIter = uint64(data.number_of_iterations);
    end
    % Update dim before building struct
    nx = nx * refinement;
    ny = ny * refinement;
    nz = nz * refinement;
    % Build struct
    model_data.analysis = data.type_of_analysis;
    model_data.dim = [ny,nx,nz];
    model_data.matProp = matProp;
    model_data.matKeys = matKeys;
    model_data.refinement = refinement;
    model_data.pcgTol = pcgTol;
    model_data.pcgIter = pcgIter;
end
%% Material map reader
function elemMatMap = readElemMatMap(filepath,model)
    elemMatMap = [];
    filepath = char(filepath);
    % local var for mesh refinement
    r = model.refinement;
    % Check if file is valid. (.tif or .raw)
    if (length(filepath) < 4), return; end
    if (filepath(end-3:end)==".tif")
        data = imread(filepath);
        if (size(data,3)<3)
            map_gray = uint16(data(:,:,1))+1;
        else
            map_gray = uint16(rgb2gray(data(:,:,1:3)))+1;
        end
        clear data
        elemMatMap = model.matKeys(map_gray(:)); clear map_gray
        if (r<=1), return; end
        if (~model.dim(3)), layers = r; else, layers = model.dim(3); end
        map = reshape(elemMatMap,model.dim(1)/r,model.dim(2)/r,layers/r);
        elemMatMap = zeros(model.dim(1),model.dim(2),model.dim(3)+uint64(model.dim(3)<=0),'uint16');
        if (model.dim(3))
            for z=1:layers/r
                aux_map = kron(map(:,:,z),ones(r,'uint16'));
                for new_layer = (z-1)*r+1:z*r
                    elemMatMap(:,:,new_layer) = aux_map;
                end
            end
        else
            elemMatMap(:,:) = kron(map(:,:),ones(r,'uint16'));
        end
        clear map aux_map
        elemMatMap = elemMatMap(:);
        return;
    end
    % If reached here, was not .tif. If not .raw, abort.
    if (filepath(end-3:end)~=".raw"), return; end
    % Read binary raw file
    fid = fopen(filepath);
    if (fid<0), return; end
    if (~model.dim(3)), layers = r; else, layers = model.dim(3); end
    data = permute(reshape(model.matKeys(uint16(fread(fid))+1),model.dim(2)/r,model.dim(1)/r,layers/r),[2,1,3]);
    fclose(fid);
    if (r<=1), elemMatMap = data(:); clear data; return; end
    elemMatMap = zeros(model.dim(1),model.dim(2),model.dim(3)+uint64(model.dim(3)<=0),'uint16');
    if (model.dim(3))
        for z=1:layers/r
            aux_map = kron(data(:,:,z),ones(r,'uint16'));
            for new_layer = (z-1)*r+1:z*r
                elemMatMap(:,:,new_layer) = aux_map;
            end
        end
    else
        elemMatMap(:,:) = kron(data(:,:),ones(r,'uint16'));
    end
    clear data aux_map
    elemMatMap = elemMatMap(:);
end
