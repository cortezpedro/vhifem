%%-------------------------------------------------------------------------
% Vectorized Homogenization with Image-based FEM (Thermal_2D)
% Input:
% * dim        = [n_rows,n_cols]
% * elemMatMap = array with material keys (this is the image)
% * matProp    = array with the isotropic conductivity of each phase
% * pcgTol     = dimensionless tolerance for the PCG method
% * pcgIter    = max number of iterations for the PCG method
% Output:
% * C = matrix representation of the homogenized constitutive tensor
%%-------------------------------------------------------------------------
function C = vhifem_Thermal_2D(dim,elemMatMap,matProp,pcgTol,pcgIter)
    C = zeros(2,2);
    %% Generate DOF map
    nElems = uint64(length(elemMatMap)); nDOFs = nElems; nRows = dim(1);
    DOFMap = zeros(dim(1)+1,dim(2)+1,'uint64');
    DOFMap(1:dim(1),1:dim(2)) = reshape(1:nDOFs,dim(1),dim(2));
    DOFMap(dim(1)+1,:) = DOFMap(1,:); DOFMap(:,dim(2)+1) = DOFMap(:,1);
    DOFMap = DOFMap(:);
    %% Initialize auxiliary arrays (for vectorized indexing and gathering)
    ee = 1:nElems; n = ee + uint64(floor(double(ee-1)/double(nRows))) + 1;
    g = zeros(nElems,1,'double');
    clear ee
    %% Compute local matrices (analytical solutions for 1x1 Q4 element)
    nMat = uint16(size(matProp,1)); prop = reshape(matProp(1:nMat),1,1,nMat);
    a = 2/3 * prop; b = -1/3 * prop; c = -1/6 * prop;
    K = [ a c b c ; c a c b ; b c a c ; c b c a ];
    a = 0.5 * prop;
    B = [ -a  a  a -a ; -a -a  a  a ];
    clear a b c prop
    %% Jacobi preconditioner
    M = zeros(nDOFs,1,'double');
    g(:) = K(1,1,elemMatMap); M(DOFMap(n)) = M(DOFMap(n)) + g;
    n = n + nRows+1;
    g(:) = K(2,2,elemMatMap); M(DOFMap(n)) = M(DOFMap(n)) + g;
    n = n - 1;
    g(:) = K(3,3,elemMatMap); M(DOFMap(n)) = M(DOFMap(n)) + g;
    n = n - (nRows+1);
    g(:) = K(4,4,elemMatMap); M(DOFMap(n)) = M(DOFMap(n)) + g;
    n = n + 1;
    M = M.^-1;
    %% Homogenization (ii=1 -> x, ii=2 -> y)
    r = zeros(nDOFs,1,'double');
    x = zeros(nDOFs,1,'double');
    d = zeros(nDOFs,1,'double');
    q = zeros(nDOFs,1,'double');
    for ii=1:2
        %% Compute RHS - Domain
        r(:) = 0;
        g(:) = B(ii,1,elemMatMap); r(DOFMap(n)) = r(DOFMap(n)) + g;
        n = n + nRows+1;
        g(:) = B(ii,2,elemMatMap); r(DOFMap(n)) = r(DOFMap(n)) + g;
        n = n - 1;
        g(:) = B(ii,3,elemMatMap); r(DOFMap(n)) = r(DOFMap(n)) + g;
        n = n - (nRows+1);
        g(:) = B(ii,4,elemMatMap); r(DOFMap(n)) = r(DOFMap(n)) + g;
        n = n + 1;
        %% Solve system (PCG - Elem by elem)
        x(:) = 0;                             % x0 = [0]
        d(:) = M.*r;                          % s  = M^-1 * r (d=s on first iteration)
        delta = dot(r,d); delta0 = delta; delta_prev = delta; % delta = dot(r,s)
        if (abs(delta)>10^-12) % check if initial guess did not already satisfy an absolute tolerance
            it = uint64(0);
            while (it < pcgIter), it = it+1;
                % q = K * d
                q(:) = 0;
                g(:) = K(1,1,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n));
                g(:) = K(1,2,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nRows+1)));
                g(:) = K(1,3,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+nRows));
                g(:) = K(1,4,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-1));
                n = n+(nRows+1);
                g(:) = K(2,1,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-(nRows+1)));
                g(:) = K(2,2,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n));
                g(:) = K(2,3,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-1));
                g(:) = K(2,4,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-(nRows+2)));
                n = n-1;
                g(:) = K(3,1,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-nRows));
                g(:) = K(3,2,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+1));
                g(:) = K(3,3,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n));
                g(:) = K(3,4,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n-(nRows+1)));
                n = n-(nRows+1);
                g(:) = K(4,1,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+1));      
                g(:) = K(4,2,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nRows+2)));
                g(:) = K(4,3,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n+(nRows+1)));
                g(:) = K(4,4,elemMatMap); q(DOFMap(n)) = q(DOFMap(n)) + g.*d(DOFMap(n));
                n = n + 1;
                a = delta / dot(d,q);         % a  = delta/dot(d,q)
                x = x + a*d;                  % x += a*d
                r = r - a*q;                  % r -= a*q
                q = M.*r;                     % s  = M^-1 * r (use q to store s)
                delta = dot(r,q);             % delta = dot(r,s)
                if (delta <= (delta0 * pcgTol^2)), break; end
                d = q + (delta/delta_prev)*d; % d = s + (delta/delta_prev)*d
                delta_prev = delta;
            end
            fprintf("PCG ran through %i steps.\nAdimensional residual: %e\n",it,sqrt(delta/delta0));
        else
            fprintf("Null solution satisfied PCG\n");
        end
        %% Update column ii of C
        qx = 0; qy = 0; t = zeros(4,1,'double'); t([1,2]+ii,:) = [1;1];
        g(:) = B(1,1,elemMatMap); qx = qx + sum(g.*(t(1)-x(DOFMap(n))));
        g(:) = B(2,1,elemMatMap); qy = qy + sum(g.*(t(1)-x(DOFMap(n))));
        n = n+(nRows+1);
        g(:) = B(1,2,elemMatMap); qx = qx + sum(g.*(t(2)-x(DOFMap(n))));
        g(:) = B(2,2,elemMatMap); qy = qy + sum(g.*(t(2)-x(DOFMap(n))));
        n = n-1;
        g(:) = B(1,3,elemMatMap); qx = qx + sum(g.*(t(3)-x(DOFMap(n))));
        g(:) = B(2,3,elemMatMap); qy = qy + sum(g.*(t(3)-x(DOFMap(n))));
        n = n-(nRows+1);
        g(:) = B(1,4,elemMatMap); qx = qx + sum(g.*(t(4)-x(DOFMap(n))));
        g(:) = B(2,4,elemMatMap); qy = qy + sum(g.*(t(4)-x(DOFMap(n))));
        n = n + 1;
        C(:,ii) = [ qx ; qy ] / double(nElems);
    end    
end
